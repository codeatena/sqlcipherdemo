package com.han.utility;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import android.os.Environment;
import android.util.Log;

public class FileUtility {
	
	public static File mainDir;
	public static File originDir;
	public static File encryptedDir;
	public static File decryptedDir;
	
	public static File originFile1;
	public static File originFile2;
	public static File originFile3;
	public static File originFile4;
	
	public static File encryptedFile1;
	public static File encryptedFile2;
	public static File encryptedFile3;
	public static File encryptedFile4;
	
	public static File decryptedFile1;
	public static File decryptedFile2;
	public static File decryptedFile3;
	public static File decryptedFile4;
	
	public static File makeDirectory(File parentFile, String dirName) {
		File directory = new File(parentFile, dirName);
		
		if (!directory.exists()) {
			if (directory.mkdirs()) {
				Log.d("CreateDir", "App dir created");
			} else{
		        Log.d("CreateDir","Unable to create app dir!");
		    }
		}
		
		return directory.getAbsoluteFile();
	}
	
	public static void makeAppDirs() {
		
    	mainDir = makeDirectory(Environment.getExternalStorageDirectory(), "RandomFiles");
    	originDir = makeDirectory(mainDir, "origin");
    	encryptedDir = makeDirectory(mainDir, "encrypted");
    	decryptedDir = makeDirectory(mainDir, "decrypted");
	}
	
	public static void makeAllFiles() {
		originFile1 = createFile(originDir, "orgin_file1.txt");
		originFile2 = createFile(originDir, "orgin_file2.txt");
		originFile3 = createFile(originDir, "orgin_file3.txt");
		originFile4 = createFile(originDir, "orgin_file4.txt");
		
		encryptedFile1 = createFile(encryptedDir, "encrypted_file1.txt");
		encryptedFile2 = createFile(encryptedDir, "encrypted_file2.txt");
		encryptedFile3 = createFile(encryptedDir, "encrypted_file3.txt");
		encryptedFile4 = createFile(encryptedDir, "encrypted_file4.txt");
		
		decryptedFile1 = createFile(decryptedDir, "decrypted_file1.txt");
		decryptedFile2 = createFile(decryptedDir, "decrypted_file2.txt");
		decryptedFile3 = createFile(decryptedDir, "decrypted_file3.txt");
		decryptedFile4 = createFile(decryptedDir, "decrypted_file4.txt");
	}
	
	public static File createFile(File parentFile, String fileName) {
		
		File file = new File(parentFile, fileName);
		
		try {
			if (!file.exists()) {
				file.createNewFile();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return file.getAbsoluteFile();
	}
	
	@SuppressWarnings("resource")
	public static void writeBytesToFile(File file, byte[] bytes) {
		try {
			FileOutputStream fos = new FileOutputStream(file);
			BufferedOutputStream bos = new BufferedOutputStream(fos);
			bos.write(bytes);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static byte[] readBytesFromFile(File file) {
		int size = (int) file.length();
	    byte[] bytes = new byte[size];
	    
	    try {
	        BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
	        buf.read(bytes, 0, bytes.length);
	        buf.close();
	    } catch (FileNotFoundException e) {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
	    } catch (IOException e) {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
	    }
	    
	    return bytes;
	}
}
