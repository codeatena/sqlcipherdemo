package com.han.utility;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

public class DialogUtility {

	public static void showGeneralAlert(Context context, String title, String message) {
		new AlertDialog.Builder(context)
	      
		   .setTitle(title)
		   .setMessage(message)
		   .setPositiveButton("OK", new DialogInterface.OnClickListener() {
			   public void onClick(DialogInterface dialog, int which) { 
		       // continue with delete
				   
		       }
		    })
		    .show();
	}
	
	public static void showToast(Context context, String msg) {
		Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
	}
}
