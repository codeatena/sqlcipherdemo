package com.han.sqlcipherdemo;

import java.io.File;
import java.util.Arrays;

import javax.crypto.SecretKey;

import com.han.utility.FileUtility;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class FileTestActivity extends Activity {

	public static int curFileIndex;
	
	File originalFile;
	File encryptedFile;
	File decryptedFile;
	
	LinearLayout llytOriginalFile;
	LinearLayout llytEncryptedFile;
	LinearLayout llytDecryptedFile;
	
	Button btnEncrypt;
	Button btnDecrypt;
	
	TextView txtOriginalFilePath;
	TextView txtOriginalFileContent;
	TextView txtEncryptedFilePath;
	TextView txtEncryptedFileContent;
	TextView txtDecryptedFilePath;
	TextView txtDecryptedFileContent;
	
	String strOriginal;
	String strEncrypted;
	String strDecrypted;
	
	SecretKey secretKey;
	byte[] saltFile;
		
	public void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
	
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_test_file);
		
		initWidget();
		initValue();
		initEvent();
	}
	
	private void initWidget() {
		llytOriginalFile = (LinearLayout) findViewById(R.id.original_file_linearLayout);
		llytEncryptedFile = (LinearLayout) findViewById(R.id.encrypted_file_linearLayout);
		llytDecryptedFile = (LinearLayout) findViewById(R.id.decrypted_file_linearLayout);
		
		btnEncrypt = (Button) findViewById(R.id.encrypt_file_button);
		btnDecrypt = (Button) findViewById(R.id.decrypt_file_button);
		
		txtOriginalFilePath = (TextView) findViewById(R.id.original_file_path_textView);
		txtOriginalFileContent = (TextView) findViewById(R.id.original_file_content_textView);
		txtEncryptedFilePath = (TextView) findViewById(R.id.encrypted_file_path_textView);
		txtEncryptedFileContent = (TextView) findViewById(R.id.encrypted_file_content_textView);
		txtDecryptedFilePath = (TextView) findViewById(R.id.decrypted_file_path_textView);
		txtDecryptedFileContent = (TextView) findViewById(R.id.decrypted_file_content_textView);
	}
	
	private void initValue() {
		llytEncryptedFile.setVisibility(View.GONE);
		btnDecrypt.setVisibility(View.GONE);
		llytDecryptedFile.setVisibility(View.GONE);	
		
		if (curFileIndex == 1) {
			originalFile = FileUtility.originFile1;
			encryptedFile = FileUtility.encryptedFile1;
			decryptedFile = FileUtility.decryptedFile1;
			secretKey = MainActivity.fileSecretKey1;
			saltFile = MainActivity.saltFile1;
		}
		if (curFileIndex == 2) {
			originalFile = FileUtility.originFile2;
			encryptedFile = FileUtility.encryptedFile2;
			decryptedFile = FileUtility.decryptedFile2;
			secretKey = MainActivity.fileSecretKey2;
			saltFile = MainActivity.saltFile2;
		}
		if (curFileIndex == 3) {
			originalFile = FileUtility.originFile3;
			encryptedFile = FileUtility.encryptedFile3;
			decryptedFile = FileUtility.decryptedFile3;
			secretKey = MainActivity.fileSecretKey3;
			saltFile = MainActivity.saltFile3;
		}
		if (curFileIndex == 4) {
			originalFile = FileUtility.originFile4;
			encryptedFile = FileUtility.encryptedFile4;
			decryptedFile = FileUtility.decryptedFile4;
			secretKey = MainActivity.fileSecretKey4;
			saltFile = MainActivity.saltFile4;
		}
		
		txtOriginalFilePath.setText(originalFile.getAbsolutePath());
		byte[] bytes = FileUtility.readBytesFromFile(originalFile);
		Log.e("getText lenth", Integer.toString(bytes.length));
		
		byte[] firstBytes = Arrays.copyOfRange(bytes, 0, 256);
		txtOriginalFileContent.setText(Crypto.toBase64(firstBytes));
		
		strOriginal = Crypto.toBase64(bytes);
	}
	
	private void initEvent() {
		btnEncrypt.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				strEncrypted = Crypto.encrypt(strOriginal, secretKey, saltFile);
				Log.e("encryted string", strEncrypted);
				
				llytEncryptedFile.setVisibility(View.VISIBLE);
				txtEncryptedFilePath.setText(encryptedFile.getAbsolutePath());
		
				byte[] bytes = strEncrypted.getBytes();
				byte[] firstBytes = Arrays.copyOfRange(bytes, 0, 256);
				txtEncryptedFileContent.setText(Crypto.toBase64(firstBytes));
				
				FileUtility.writeBytesToFile(encryptedFile, bytes);
				btnDecrypt.setVisibility(View.VISIBLE);
			}
		});
		
		btnDecrypt.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				
				byte[] bytes = FileUtility.readBytesFromFile(encryptedFile);
				strEncrypted = new String(bytes);
				
				strDecrypted = Crypto.decryptPbkdf2(strEncrypted, MainActivity.userPassphrase);
				
				Log.e("strDecrypted", strDecrypted);
				
				llytDecryptedFile.setVisibility(View.VISIBLE);
				txtDecryptedFilePath.setText(decryptedFile.getAbsolutePath());
				
				bytes = Crypto.fromBase64(strDecrypted);
				Log.e("decrypted string length", Integer.toString(bytes.length));
				
				byte[] firstBytes = Arrays.copyOfRange(bytes, 0, 256);
				txtDecryptedFileContent.setText(Crypto.toBase64(firstBytes));
				
				FileUtility.writeBytesToFile(decryptedFile, bytes);
			}
		});
	}
}
