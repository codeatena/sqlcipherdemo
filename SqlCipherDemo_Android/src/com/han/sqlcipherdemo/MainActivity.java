package com.han.sqlcipherdemo;

import javax.crypto.SecretKey;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    
    public static SecretKey mainSecretKey;
    public static SecretKey fileSecretKey1;
    public static SecretKey fileSecretKey2;
    public static SecretKey fileSecretKey3;
    public static SecretKey fileSecretKey4;
    
    public static byte[] saltMain;
    public static byte[] saltFile1;
    public static byte[] saltFile2;
    public static byte[] saltFile3;
    public static byte[] saltFile4;
    
    public static String userPassphrase;
    
    abstract class Encryptor {
        SecretKey key;

        abstract SecretKey deriveKey(String passpword, byte[] salt);

        abstract String encrypt(String plaintext, String password);

        abstract String decrypt(String ciphertext, String password);

        String getRawKey() {
            if (key == null) {
                return null;
            }
            return Crypto.toHex(key.getEncoded());
        }
    }

    private final Encryptor PBKDF2_ENCRYPTOR = new Encryptor() {

        @Override
        public SecretKey deriveKey(String password, byte[] salt) {
        	key = Crypto.deriveKeyPbkdf2(salt, password);
            return key;
        }

        @Override
        public String encrypt(String plaintext, String password) {
            byte[] salt = Crypto.generateSalt();
            //key = deriveKey(password, salt);
            Log.d(TAG, "Generated key: " + getRawKey());

            return Crypto.encrypt(plaintext, key, salt);
        }

        @Override
        public String decrypt(String ciphertext, String password) {
            return Crypto.decryptPbkdf2(ciphertext, password);
        }
    };

    private EditText passwordText;
    
    TextView txtMainKey;
    TextView txtFileKey1;
    TextView txtFileKey2;
    TextView txtFileKey3;
    TextView txtFileKey4;
    
    Button btnGenerateKeys;
    Button btnCreateFiles;
    
    private Encryptor encryptor;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setProgressBarIndeterminateVisibility(false);

        initWidget();
        initValue();
        initEvent();
    }
    
    private void initWidget() {
        passwordText = findById(R.id.password_text);
        
        txtMainKey = findById(R.id.main_key_text);
        txtFileKey1 = findById(R.id.file_key_text1);
        txtFileKey2 = findById(R.id.file_key_text2);
        txtFileKey3 = findById(R.id.file_key_text3);
        txtFileKey4 = findById(R.id.file_key_text4);

        btnGenerateKeys = findById(R.id.generate_key_button);
        btnCreateFiles = findById(R.id.create_files_button);
    }
    
    private void initValue() {
        encryptor = PBKDF2_ENCRYPTOR;
        btnCreateFiles.setVisibility(View.GONE);
    }
    
    private void initEvent() {
    	btnGenerateKeys.setOnClickListener(this);
    	btnCreateFiles.setOnClickListener(this);
    }

    @SuppressWarnings("unchecked")
    private <T> T findById(int id) {
        return (T) findViewById(id);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == btnGenerateKeys.getId()) {
            final String password = passwordText.getText().toString().trim();
            if (password.length() == 0) {
                Toast.makeText(this, "Please enter a password.",
                        Toast.LENGTH_SHORT).show();
                return;
            }
            
            saltMain = Crypto.generateSalt();
            mainSecretKey = encryptor.deriveKey(password, saltMain);
            txtMainKey.setText(Crypto.toBase64(mainSecretKey.getEncoded()));
            
            saltFile1 = Crypto.generateSalt();
            fileSecretKey1 = encryptor.deriveKey(password, saltFile1);
            txtFileKey1.setText(Crypto.toBase64(fileSecretKey1.getEncoded()));
            
            saltFile2 = Crypto.generateSalt();
            fileSecretKey2 = encryptor.deriveKey(password, saltFile2);
            txtFileKey2.setText(Crypto.toBase64(fileSecretKey2.getEncoded()));
            
            saltFile3 = Crypto.generateSalt();
            fileSecretKey3 = encryptor.deriveKey(password, saltFile3);
            txtFileKey3.setText(Crypto.toBase64(fileSecretKey3.getEncoded()));
            
            saltFile4 = Crypto.generateSalt();
            fileSecretKey4 = encryptor.deriveKey(password, saltFile4);
            txtFileKey4.setText(Crypto.toBase64(fileSecretKey4.getEncoded()));
            
            btnCreateFiles.setVisibility(View.VISIBLE);
        }
        if (v.getId() == btnCreateFiles.getId()) {
        	userPassphrase = passwordText.getText().toString().trim();
        	startActivity(new Intent(this, FileCreateActivity.class));
        }
    }
}