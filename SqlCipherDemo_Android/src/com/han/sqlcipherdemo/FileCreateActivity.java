package com.han.sqlcipherdemo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import net.sqlcipher.database.SQLiteDatabase;

import com.han.utility.DialogUtility;
import com.han.utility.FileUtility;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

public class FileCreateActivity extends Activity implements OnClickListener {
	
	Button btnFile1;
	Button btnFile2;
	Button btnFile3;
	Button btnFile4;
	
	Button btnReadDB;
	Button btnWriteDB;
	
	TextView txtCurrentKeys;
	TextView txtDBContent;
	
	EventDataSQLHelper eventsData;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
	
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_create_file);
	 
		initWidget();
		initValue();
		initEvent();
	}
	 
	private void initWidget() {
		btnFile1 = (Button) findViewById(R.id.file1_button);
		btnFile2 = (Button) findViewById(R.id.file2_button);
		btnFile3 = (Button) findViewById(R.id.file3_button);
		btnFile4 = (Button) findViewById(R.id.file4_button);
		
		btnReadDB = (Button) findViewById(R.id.read_data_button);
		btnWriteDB = (Button) findViewById(R.id.write_data_button);
		
		txtCurrentKeys = (TextView) findViewById(R.id.current_keys_textView);
		txtDBContent = (TextView) findViewById(R.id.database_content_textView);
	}
	
	private void initValue() {
    	FileUtility.makeAppDirs();
    	FileUtility.makeAllFiles();
    	
    	FileUtility.writeBytesToFile(FileUtility.originFile1, Crypto.getRandomFileContent());
    	FileUtility.writeBytesToFile(FileUtility.originFile2, Crypto.getRandomFileContent());
    	FileUtility.writeBytesToFile(FileUtility.originFile3, Crypto.getRandomFileContent());
    	FileUtility.writeBytesToFile(FileUtility.originFile4, Crypto.getRandomFileContent());
    	
    	initCurrentKeys();
    	initDatabase();
	}
	
	private void initCurrentKeys() {
		StringBuilder ret = new StringBuilder("\nGenerated new keys:\n\n");
		String strKeyName, strKeyValue, strSalt;
		
		strKeyName = "main_key";
		strKeyValue = Crypto.toBase64(MainActivity.mainSecretKey.getEncoded());
		strSalt = Crypto.toBase64(MainActivity.saltMain);
		ret.append(strKeyName + ": " + strKeyValue + "(" + strSalt + ")\n");
		
		strKeyName = "file_key1";
		strKeyValue = Crypto.toBase64(MainActivity.fileSecretKey1.getEncoded());
		strSalt = Crypto.toBase64(MainActivity.saltFile1);
		ret.append(strKeyName + ": " + strKeyValue + "(" + strSalt + ")\n");
		
		strKeyName = "file_key2";
		strKeyValue = Crypto.toBase64(MainActivity.fileSecretKey2.getEncoded());
		strSalt = Crypto.toBase64(MainActivity.saltFile2);
		ret.append(strKeyName + ": " + strKeyValue + "(" + strSalt + ")\n");
		
		strKeyName = "file_key3";
		strKeyValue = Crypto.toBase64(MainActivity.fileSecretKey3.getEncoded());
		strSalt = Crypto.toBase64(MainActivity.saltFile3);
		ret.append(strKeyName + ": " + strKeyValue + "(" + strSalt + ")\n");
		
		strKeyName = "file_key4";
		strKeyValue = Crypto.toBase64(MainActivity.fileSecretKey4.getEncoded());
		strSalt = Crypto.toBase64(MainActivity.saltFile4);
		ret.append(strKeyName + ": " + strKeyValue + "(" + strSalt + ")\n");
		
		txtCurrentKeys.setText(ret.toString());
	}

	private void initEvent() {
		btnFile1.setOnClickListener(this);
		btnFile2.setOnClickListener(this);
		btnFile3.setOnClickListener(this);
		btnFile4.setOnClickListener(this);
		
		btnReadDB.setOnClickListener(this);
		btnWriteDB.setOnClickListener(this);
	}
	
    public void onClick(View v) {
		if (v == btnFile1) {
			FileTestActivity.curFileIndex = 1;
			startActivity(new Intent(this, FileTestActivity.class));
		}
		if (v == btnFile2) {
			FileTestActivity.curFileIndex = 2;
			startActivity(new Intent(this, FileTestActivity.class));
		}
		if (v == btnFile3) {
			FileTestActivity.curFileIndex = 3;
			startActivity(new Intent(this, FileTestActivity.class));
		}
		if (v == btnFile4) {
			FileTestActivity.curFileIndex = 4;
			startActivity(new Intent(this, FileTestActivity.class));
		}
		if (v == btnReadDB) {
			readKeyData();
		}
		if (v == btnWriteDB) {
			writeKeyData();
		}
	}
    
    private void initDatabase() {
    	SQLiteDatabase.loadLibs(this);

//    	File databaseFile = getDatabasePath("demo.db");
//      databaseFile.mkdirs();
//      databaseFile.delete();
//      SQLiteDatabase database = SQLiteDatabase.openOrCreateDatabase(databaseFile, "test123", null);
    	
        eventsData = new EventDataSQLHelper(this);
    }
    
    private void writeKeyData() {
        String password = MainActivity.userPassphrase;
    	SQLiteDatabase db = eventsData.getWritableDatabase(password);
    	
        db.delete(EventDataSQLHelper.TABLE_KEY, null, null);

        ContentValues values = new ContentValues();
        values.put(EventDataSQLHelper.FIEILD_KEY_NAME, "main_key");
        values.put(EventDataSQLHelper.FIEILD_KEY_VALUE, Crypto.toBase64(MainActivity.mainSecretKey.getEncoded()));
        values.put(EventDataSQLHelper.FIEILD_KEY_SALT, Crypto.toBase64(MainActivity.saltMain));
        db.insert(EventDataSQLHelper.TABLE_KEY, null, values);
        
        values = new ContentValues();
        values.put(EventDataSQLHelper.FIEILD_KEY_NAME, "file_key1");
        values.put(EventDataSQLHelper.FIEILD_KEY_VALUE, Crypto.toBase64(MainActivity.fileSecretKey1.getEncoded()));
        values.put(EventDataSQLHelper.FIEILD_KEY_SALT, Crypto.toBase64(MainActivity.saltFile1));
        db.insert(EventDataSQLHelper.TABLE_KEY, null, values);
        
        values = new ContentValues();
        values.put(EventDataSQLHelper.FIEILD_KEY_NAME, "file_key2");
        values.put(EventDataSQLHelper.FIEILD_KEY_VALUE, Crypto.toBase64(MainActivity.fileSecretKey2.getEncoded()));
        values.put(EventDataSQLHelper.FIEILD_KEY_SALT, Crypto.toBase64(MainActivity.saltFile2));
        db.insert(EventDataSQLHelper.TABLE_KEY, null, values);
        
        values = new ContentValues();
        values.put(EventDataSQLHelper.FIEILD_KEY_NAME, "file_key3");
        values.put(EventDataSQLHelper.FIEILD_KEY_VALUE, Crypto.toBase64(MainActivity.fileSecretKey3.getEncoded()));
        values.put(EventDataSQLHelper.FIEILD_KEY_SALT, Crypto.toBase64(MainActivity.saltFile3));
        db.insert(EventDataSQLHelper.TABLE_KEY, null, values);
        
        values = new ContentValues();
        values.put(EventDataSQLHelper.FIEILD_KEY_NAME, "file_key4");
        values.put(EventDataSQLHelper.FIEILD_KEY_VALUE, Crypto.toBase64(MainActivity.fileSecretKey4.getEncoded()));
        values.put(EventDataSQLHelper.FIEILD_KEY_SALT, Crypto.toBase64(MainActivity.saltFile4));
        db.insert(EventDataSQLHelper.TABLE_KEY, null, values);
        
        db.close();
        copyDatabaseFile();
        DialogUtility.showToast(this, "New keys are saved on database.");
    }
    
    private void readKeyData() {
        String password = MainActivity.userPassphrase;
        SQLiteDatabase db = eventsData.getReadableDatabase(password);
        
        Cursor cursor = db.query(EventDataSQLHelper.TABLE_KEY, null, null, null, null, null, null);
            
        StringBuilder ret = new StringBuilder("\nSaved Keys on Database:\n\n");
        while (cursor.moveToNext()) {
        	String strKeyName = cursor.getString(1);
        	String strKeyValue = cursor.getString(2);
        	String strSalt = cursor.getString(3);
        	ret.append(strKeyName + ": " + strKeyValue + "(" + strSalt + ")\n");
        }
        
        db.close();
        Log.e("read data content", ret.toString());
        txtDBContent.setText(ret.toString());
    }
    
    public void copyDatabaseFile(){
    	
        File file = this.getDatabasePath(EventDataSQLHelper.DATABASE_NAME);
        
        Log.e("database file path: ", file.getAbsolutePath());
        try {
        	//Open your local db as the input stream                
        	InputStream myInput = new FileInputStream(file.getAbsolutePath());
        
        	// Path to the just created empty db
        	String outFileName = Environment.getExternalStorageDirectory() + "/RandomFiles/" + EventDataSQLHelper.DATABASE_NAME;
        	Log.e("sd card file path: ", outFileName);
        	//	Open the empty db as the output stream
        	OutputStream myOutput;
        
	        myOutput = new FileOutputStream(outFileName);
	        //transfer bytes from the inputfile to the outputfile
	        byte[] buffer = new byte[1024];
	        int length;
	        while ((length = myInput.read(buffer))>0){
	        	myOutput.write(buffer, 0, length);
	        }
	        //Close the streams
	        myOutput.flush();
	        myOutput.close();
	        myInput.close();
        } catch (FileNotFoundException e) {
        	// 	TODO Auto-generated catch block
        	e.printStackTrace();
        } catch (IOException e) {
        	// 	TODO Auto-generated catch block
        	e.printStackTrace();
        }
    }
}