package com.han.sqlcipherdemo;

import net.sqlcipher.database.SQLiteDatabase;
import net.sqlcipher.database.SQLiteOpenHelper;
import android.content.Context;
import android.os.Environment;
import android.provider.BaseColumns;
import android.util.Log;

/** Helper to the database, manages versions and creation */
public class EventDataSQLHelper extends SQLiteOpenHelper {
	public static final String DATABASE_NAME = "main.db";
	public static final int DATABASE_VERSION = 1;

	// Table name
	public static final String TABLE_KEY = "security_key";

	// Columns
	public static final String FIEILD_KEY_NAME = "name";
	public static final String FIEILD_KEY_VALUE = "value";
	public static final String FIEILD_KEY_SALT = "salt";
	
	public EventDataSQLHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		String sql = "create table " + TABLE_KEY + "( " + BaseColumns._ID
				+ " integer primary key autoincrement, " +
				FIEILD_KEY_NAME + " text not null, " +
				FIEILD_KEY_VALUE + " text not null, " +
				FIEILD_KEY_SALT + " text not null);";
		
		Log.d("EventsData", "onCreate: " + sql);
		db.execSQL(sql);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		if (oldVersion >= newVersion)
			return;

		String sql = null;
		if (oldVersion == 1) 
			sql = "alter table " + TABLE_KEY + " add note text;";
		if (oldVersion == 2)
			sql = "";

		Log.d("EventsData", "onUpgrade	: " + sql);
		if (sql != null)
			db.execSQL(sql);
	}
}
